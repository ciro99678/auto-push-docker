#!/bin/sh
git clone ${GIT_REPO}
cd auto-push-docker
docker login registry.gitlab.com -u ${username} -p ${password}

docker build --no-cache -t registry.gitlab.com/ciro99678/auto-push-docker:angular -f "Dockerfile.step" .
docker push registry.gitlab.com/ciro99678/auto-push-docker:angular

cd express/api-server
docker build --no-cache -t registry.gitlab.com/ciro99678/auto-push-docker:express -f "Dockerfile" .
docker push registry.gitlab.com/ciro99678/auto-push-docker:express