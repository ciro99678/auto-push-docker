FROM docker:20.10.20-alpine3.16
WORKDIR /app
ENV GIT_REPO="http://gitlab.com/ciro99678/auto-push-docker.git"
ENV username="ciro99678"
ENV password=""
RUN apk update && apk add bash
RUN apk --no-cache add git
WORKDIR /app/auto-push-docker
COPY exe.sh /app/auto-push-docker
ENTRYPOINT [ "/app/auto-push-docker/exe.sh" ]