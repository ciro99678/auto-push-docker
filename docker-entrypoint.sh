#!/bin/sh

set -e
chmod +x /var/www/localhost/htdocs

mv /app/auto-push-docker/dist/* /var/www/localhost/htdocs/

httpd -D FOREGROUND